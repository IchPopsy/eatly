const SignUpModal = document.querySelector(".signup_modal"); // модалка сигнапа
const spanSignUp = document.querySelector(".span_signup"); // кнопка сигнапа в модалке логина
const BtnSignUp = document.querySelector(".header_signup"); // кнопка сигнапа в хедере
const BtnSignUpMobile = document.querySelector(".header_signup_mobile"); // сигнап в мобилке
const LoginModal = document.querySelector(".login_modal"); // модалка логина
const spanLogin = document.querySelector(".span_login"); // кнопка логина в модалке сигнапа
const BtnLogin = document.querySelector(".header_login"); // кнопка логина в хедере
const BtnLoginMobile = document.querySelector(".header_login_mobile"); // логин в мобилке
const SignUpClose = document.querySelector(".signupclose"); // кнопка закрытия sign
const LoginClose = document.querySelector(".loginclose"); // кнопка закрытия login

const SignUpCloseMobile = document.querySelector(".mobileclosesignup"); // кнопка закрытия sign mobile
const LoginCloseMobile = document.querySelector(".mobilecloselogin"); // кнопка закрытия login mobile

const togglePassword = document.querySelector("#togglePassword"); // переменная для пароля
const password = document.querySelector("#password"); // переменная пароля
const openEye = document.querySelector(".openeye");
const TogglePassword = document.getElementById("togglePassword");

BtnSignUp.addEventListener("click", () => {
  SignUpModal.style.display = "block";
  document.body.style.overflow = "hidden";
});

BtnSignUpMobile.addEventListener("click", () => {
  SignUpModal.style.display = "block";
  document.body.style.overflow = "hidden";
});

spanSignUp.addEventListener("click", () => {
  LoginModal.style.display = "none";
  SignUpModal.style.display = "block";
  document.body.style.overflow = "hidden";
});

SignUpClose.addEventListener("click", () => {
  SignUpModal.style.display = "none";
  document.body.style.overflow = "visible";
});

BtnLogin.addEventListener("click", () => {
  LoginModal.style.display = "block";
  document.body.style.overflow = "hidden";
});

SignUpCloseMobile.addEventListener("click", () => {
  SignUpModal.style.display = "none";
  document.body.style.overflow = "visible";
});

BtnLoginMobile.addEventListener("click", () => {
  LoginModal.style.display = "block";
  document.body.style.overflow = "hidden";
});

spanLogin.addEventListener("click", () => {
  SignUpModal.style.display = "none";
  LoginModal.style.display = "block";
  document.body.style.overflow = "hidden";
});

LoginClose.addEventListener("click", () => {
  LoginModal.style.display = "none";
  document.body.style.overflow = "visible";
});

LoginCloseMobile.addEventListener("click", () => {
  LoginModal.style.display = "none";
  document.body.style.overflow = "visible";
});

togglePassword.addEventListener("click", function () {
  const type =
    password.getAttribute("type") === "password" ? "text" : "password";
  password.setAttribute("type", type);
});

TogglePassword.addEventListener("click", function () {
  openEye.classList.toggle("hidden");
});

// 2. При клике на глазик togglePassword глазик должен становиться открытый
// 3. Адаптив header
// у меня блять нахуй епта нахуй сломался переключатель видимости епта пароля ебана пажжи у СИГНАПА, не у логина ебана
// а ещё не
// 4. Сделать адаптив для модалок

// Переключение мобильного меню бургера

const burgerBtn = document.querySelector(".burger");
const mobileMenu = document.querySelector(".header_menu_mobile");

burgerBtn.addEventListener("click", () => {
  burgerBtn.classList.toggle("active");
  mobileMenu.classList.toggle("active");
});
